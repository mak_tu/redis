'use strict';

const {createClient} = require('redis'),
      worker = createClient(),
      client = createClient(),
      storage = createClient(),
      time = new Date(),
      id = `id${+time}`; //уникальный id воркера

worker.on('message', (channel, message) => {
    const probability = Math.random();
    if (channel === id && probability < 0.95) { //пришло корректное сообщение
        client.publish('response', message);
    } else if(channel === id && probability > 0.95) { //пришла ошибка
        client.publish('response', 'Error!');
        const err = JSON.stringify({id, time});
        storage.lpush('queue[errors]', err);
    } else if(channel === 'end') { //убить воркер
        proccess.exit(0);
    } else {
        //
    }
});

client.publish('add', id);

worker.subscribe(id);
worker.subscribe('end');