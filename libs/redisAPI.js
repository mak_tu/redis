class RedisAPI {
    constructor(redis) {
        this.masterID = new Array(); //здесь хранятся уникальные ID всех воркеров
        this.master = redis.createClient();
    }

    addWorker(id) {
        this.masterID.push(id);
        console.log(`Worker ${id} is working!`);
    }

    //отправление сообщения на обработку одному из воркеру
    emit(string) {
        if (this.masterID.length > 0) {
            let needID = this.masterID.splice(0, 1)[0];
            this.master.publish(needID, string);
            this.masterID.push(needID);
        } else {
            setTimeout(() => {
                this.emit(string);
            }, 100);
        }

    }

    //отправление сообщения на обработку одному из воркеров каждые ms секунд
    emitTimeout(string, ms) {
        setInterval(() => {
            this.emit(string);
        }, ms);
    }

    //вывод всех ошибок на экран
    printError() {
        const that = this;
        this.master.lrange('queue[errors]', '0', '-1', (err, data) => {
            if (err) throw err;
            console.dir(data, {colors: true});
            that.master.publish('end');
            that.master.del('queue');
            process.exit(0);
        })
    }
}

module.exports = RedisAPI;