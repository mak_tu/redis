'use strict';

const redis = require('redis'),
      listener = redis.createClient(),
      publisher = redis.createClient(),
      stdin = process.openStdin(),
      RedisAPI = require('./libs/redisAPI'),
      {exec} = require('child_process'),
      queue = new RedisAPI(redis);

listener.on('message', (channel, message) => {
    channel === 'add' ? queue.addWorker(message) : console.log(message);
});

listener.subscribe('add');
listener.subscribe('response');

stdin.addListener("data", function(d) {
    switch (d.toString()) {
        case '-a\n':
            exec('node master.js');
        break;

        case '-e\n':
            publisher.publish('end', '');
            queue.printError();
        break;

        case '-s\n':
            publisher.publish('end', '');
            process.exit(0);
        break;

        default:
            //
        break;
    }
});

queue.emitTimeout('Good message', 2000);
console.log('Приложение запущено. Чтобы добавить нового воркера, введите -a')